package com.greglturnquist.payroll;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class EmployeeTest {

    Employee employee = new Employee("Frodo", "Baggins", "Hobbit", "Ring Bearer");

    @org.junit.jupiter.api.Test
    void testEqualsSame() {
        assertEquals(employee, employee);
    }

    @org.junit.jupiter.api.Test
    void testEqualsDifferentObject() {
        assertNotEquals(employee, new ArrayList<String>());
    }

    @org.junit.jupiter.api.Test
    void testEqualsNull() {
        assertNotEquals(employee, null);
    }

    @org.junit.jupiter.api.Test
    void testEqualsEqual() {
        Employee employee2 = new Employee("Frodo", "Baggins", "Hobbit", "Ring Bearer");
        assertEquals(employee, employee2);
    }

    @org.junit.jupiter.api.Test
    void testHashCode() {
        Employee employee2 = new Employee("Frodo", "Baggins", "Hobbit", "Ring Bearer");
        assertEquals(employee.hashCode(), employee2.hashCode());
    }

    @org.junit.jupiter.api.Test
    void setId() {
        employee.setId(5L);
        long expected = 5;
        long actual = employee.getId();
        assertEquals(expected, actual);
    }

    @org.junit.jupiter.api.Test
    void setFirstName() {
        employee.setFirstName("Aragon");
        assertEquals("Aragon", employee.getFirstName());
    }

    @org.junit.jupiter.api.Test
    void setLastName() {
        employee.setLastName("Something");
        assertEquals("Something", employee.getLastName());
    }

    @org.junit.jupiter.api.Test
    void setDescription() {
        employee.setDescription("Soldier");
        assertEquals("Soldier", employee.getDescription());
    }

    @org.junit.jupiter.api.Test
    void setJobTitle() {
        employee.setJobTitle("Mercenary");
        assertEquals("Mercenary", employee.getJobTitle());
    }

    @org.junit.jupiter.api.Test
    void testToString() {
        String expected = "Employee{id=null, firstName='Frodo', lastName='Baggins', description='Hobbit', jobTitle='Ring Bearer'}";
        assertEquals(expected, employee.toString());
    }
}